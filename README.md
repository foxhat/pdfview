# README #

### What is this repository for? ###
This repository holds the source code for the PDFView rendering library.
The library will be a light weight, fast, portable library for rendering
PDF documents and data streams.

### Version
This is [version][2] 0.1.0.
This is using a [gyp][3] generated Visual Studio 2008 (VS2008) solution.
This has also been tested on VS2010, VS2012, VS2013, and VS2015.

When you build with a later version of Visual Studio, the project is
upgraded to suit the version you are using. 

### How do I get set up? ###

Just open `pdfium.sln` in Visual Studio, and select `Build->Solution`.
This will produce a bunch of static libraries in the `build\Debug\lib` directory.
The header files you need are in `.\fpdfsdk\include`.

You can [download][4] the API documentation from Foxit Software.

### Caveats

Visual Studio will report a number of warnings. These are restricted to one
or two files, and are on my hit list.

#### Runtime dependency

At sometime before the library was released as Open Source, the font rendering
code for the Windows platform was changed. As a result, code linked to the
library must be running on:

* Windows 7 or Windows Vista with Service Pack 2 (SP2) and Platform Update for
Windows Vista
* Windows Server 2008 R2 or Windows Server 2008 with Service Pack 2 (SP2) and
Platform Update for Windows Server 2008
* Any later supported version of Windows.

### Roadmap

In no particular order:

* Warning free builds.

* Switch the build to CMake. CMake is mature, stable, and being actively
maintained. However, you won't need CMake to build the libraries.

* Doxygen documentation.
    * Browsable web of internals.
    * New API documentation

* Support other platforms.

* Tested, application code. I'm aiming at a portable PDF reader that
demonstrates all of the capabilities of the library.

* Project Infrastructure
    * Web site
    * Tickets
    * Wiki
    * ...

### Contribution guidelines ###

* Coming real soon now!

### Who do I talk to? ###

* Coming real soon now!

Written 4 March 2017. The links were current on that date.

[1]: https://pdfium.googlesource.com/pdfium/
[2]: http://semver.org/
[3]: https://gyp.gsrc.io/
[4]: http://cdn01.foxitsoftware.com/pub/foxit/manual/enu/FoxitPDF_SDK20_Guide.pdf