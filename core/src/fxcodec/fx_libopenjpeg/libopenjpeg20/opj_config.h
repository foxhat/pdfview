// Copyright 2014 PDFium Authors. All rights reserved.
// Use of this source code is governed by a BSD-style license that can be
// found in the LICENSE file.

// Original code copyright 2014 Foxit Software Inc. http://www.foxitsoftware.com

/* create opj_config.h for CMake */
// TODO. Fix in CMake. The issue with the original was that it introduced a
// dependency on the Platform SDK. It might not be installed on Joe's box
//#if defined(_MSC_VER) && _MSC_VER < 1700

#if defined(_MSC_VER)
#define OPJ_HAVE_INTTYPES_H 	0
#else
#define OPJ_HAVE_INTTYPES_H 	1
#endif

/*--------------------------------------------------------------------------*/
/* OpenJPEG Versioning                                                      */

/* Version number. */
#define OPJ_VERSION_MAJOR 2
#define OPJ_VERSION_MINOR 1
#define OPJ_VERSION_BUILD 0
